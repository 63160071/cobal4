       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMI.
       AUTHOR. CindyCheeseCake.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WEIGHT       PIC 9(3).
       01 HEIGHT       PIC 9(3).
       01 BMI          PIC 9(2)V9(2).

       PROCEDURE DIVISION.
           DISPLAY "Enter Weight (in kilograms): ".
           ACCEPT WEIGHT.
       
           DISPLAY "Enter Height (in meters): ".
           ACCEPT HEIGHT.
       
           COMPUTE BMI = WEIGHT / (HEIGHT * HEIGHT).
       
           DISPLAY "Your BMI = : " BMI.
       
           IF BMI < 18.5
               DISPLAY "You are underweight."     
           ELSE IF BMI >= 18.5 AND BMI < 24.9
               DISPLAY "You are normal weight."
           ELSE IF BMI >= 25.0 AND BMI < 29.9
               DISPLAY "You are overweight."
           ELSE
               DISPLAY "You are obese."
           END-IF.
       
           STOP RUN.
